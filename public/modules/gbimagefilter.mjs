"use strict";
import * as GLHelper from "./gl_helper.mjs";
import { RangeInputs } from "./range_inputs.mjs";

export class GBImageFilter {
  #div = null;
  #wrap = document.createElement("wrap");
  #canvas = document.createElement("canvas");
  #gl = this.#canvas.getContext("webgl2");
  #frame_buffer = null;
  #program = null;
  #texture = {data : null, width : 0, height : 0};
  #vao = null;
  #object = null;
  #screen = null;
  #params = null;
  #zoom = 1.0;
  #scroll = {x : 0.0, y : 0.0};

  constructor(div, resolution, scale) {
    this.#div = div;
    this.#wrap.style.display = "flex";
    this.#div.appendChild(this.#wrap);
    this.#canvas.width = resolution[0] * scale;
    this.#canvas.height = resolution[1] * scale;
    this.#canvas.style.border = "2px solid";
    this.#frame_buffer = GLHelper.CreateRendererTexture(this.#gl, resolution);
    this.#program = this.#CreateRendererProgram(resolution);
    this.#SetDummyImage(0.0, 1.0, 0.0);
    this.#vao = this.#gl.createVertexArray();
    this.#gl.bindVertexArray(this.#vao);
    this.#object = GLHelper.CreatePanelObject(this.#gl);
    this.#gl.bindVertexArray(null);
    this.#screen = new GLHelper.TextureScreen(this.#gl);
    const input_recipe = [
      {min : -1, max : 1, step : 0.01, init : 0},
      {min :  0, max : 1, step : 0.01, init : [0.33, 0.67, 1.0]},
      {min :  0, max : 1, step : 0.01, init : 0.5},
      {min :  0, max : 1, step : 0.01, init : 0.5}
    ];
    this.#params = new RangeInputs(this.#wrap, input_recipe);
    this.#params.SetCallback(() => this.Draw());
    this.#wrap.appendChild(this.#canvas);
    this.Draw()
  }

  #CreateRendererProgram(resolution) {
    let vertex_source =
      `#version 300 es
      layout (location = 0) in vec3 _vp;
      layout (location = 1) in vec3 _vn;
      layout (location = 2) in vec3 _vc;
      void main(void) {
        gl_Position = vec4(_vp, 1.0);
      }`;
    let fragment_source =
      `#version 300 es
      precision mediump float;
      uniform sampler2D _pict;
      uniform vec2 _pict_res;
      uniform float _zoom;
      uniform vec2 _scroll;
      uniform float _pre_lumi_fix;
      uniform vec3 _tone_thr;
      uniform float _trans_lumi;
      uniform float _bayer_range;
      const vec2 _res = vec2(${resolution[0].toFixed(1)}, ${resolution[1].toFixed(1)});
      #define saturate(x) clamp(x, 0.0, 1.0)
      layout (location = 0) out vec4 _color;

      vec2 GetRatioFixer(in vec2 view_res, in vec2 tex_res) {
        vec2 ratio = _res / _pict_res;
        float factor = min(ratio.x, ratio.y);
        return ratio / factor;
      }

      vec2 GetPositionFixer(in vec2 ratio_fixer) {
        return (ratio_fixer - vec2(1.0)) * 0.5;
      }

      float GetUVMask(in vec2 uv) {
        vec2 tmp = step(vec2(0.0), uv) * step(uv, vec2(1.0));
        return tmp.x * tmp.y;
      }

      float GetLuminance(in vec3 color) {
        return 0.299 * color.r + 0.587 * color.g + 0.114 * color.b;
      }

      float GetBayerThr(in vec2 coord) {
        const mat4 bayer = mat4( 0.0, 12.0,  3.0, 15.0,
                                 8.0,  4.0, 11.0,  7.0,
                                 2.0, 14.0,  1.0, 13.0,
                                10.0,  6.0,  9.0,  5.0);
        vec2 block = step(4.0, mod(coord, vec2(8.0)));
        float add = 3.0 * block.x + 2.0 * block.y - 5.0 * block.x * block.y;
        return (4.0 * bayer[int(mod(coord.y, 4.0))][int(mod(coord.x, 4.0))] + add) / 64.0;
      }

      void main(void) {
        vec2 ratio_fixer = GetRatioFixer(_res, _pict_res);
        vec2 pos_fixer = GetPositionFixer(ratio_fixer);
        vec2 uv = ((gl_FragCoord.xy / _res) * ratio_fixer - pos_fixer) * _zoom - _scroll;
        vec4 pict_color = texture(_pict, uv) * GetUVMask(uv);
        float lumi = saturate(GetLuminance(pict_color.rgb) + _pre_lumi_fix);
        lumi = lumi * step(0.5, pict_color.a) +
               _trans_lumi * step(pict_color.a, 0.5);
        // float tone = floor(lumi * 3.0) / 3.0;
        int tone_index = lumi < _tone_thr[0] ? 0 :
                         lumi < _tone_thr[1] ? 1 :
                         lumi < _tone_thr[2] ? 2 :
                         3;
        float tone = (1.0 / 3.0) * float(tone_index);
        float curve = tone_index == 0 ? lumi / _tone_thr[0] :
                      tone_index == 1 ? (lumi - _tone_thr[0]) / (_tone_thr[1] - _tone_thr[0]) :
                      tone_index == 2 ? (lumi - _tone_thr[1]) / (_tone_thr[2] - _tone_thr[1]) :
                      (lumi - _tone_thr[2]) / (1.0 - _tone_thr[2]);
        float bayer_thr = GetBayerThr(gl_FragCoord.xy);
        float color_thr = curve + (bayer_thr - 0.5) * _bayer_range;
        _color = vec4(vec3(step(0.5, color_thr) * (1.0 / 3.0) + tone), 1.0);

        // _color = vec4(vec3(tone), 1.0);

        // _color = vec4(vec3(step(0.5, color_thr)), 1.0);
      }`;
    const vertex_shader = GLHelper.CreateVertexShader(this.#gl, vertex_source);
    const fragment_shader = GLHelper.CreateFragmentShader(this.#gl, fragment_source);
    const program = GLHelper.CreateProgram(this.#gl, vertex_shader, fragment_shader);
    const pict = this.#gl.getUniformLocation(program, "_pict");
    const pict_res = this.#gl.getUniformLocation(program, "_pict_res");
    const zoom = this.#gl.getUniformLocation(program, "_zoom");
    const scroll = this.#gl.getUniformLocation(program, "_scroll");
    const pre_lumi_fix = this.#gl.getUniformLocation(program, "_pre_lumi_fix");
    const tone_thr = this.#gl.getUniformLocation(program, "_tone_thr");
    const trans_lumi = this.#gl.getUniformLocation(program, "_trans_lumi");
    const bayer_range = this.#gl.getUniformLocation(program, "_bayer_range");
    return {
      program : program,
      pict : pict,
      pict_res: pict_res,
      zoom : zoom,
      scroll : scroll,
      pre_lumi_fix : pre_lumi_fix,
      tone_thr : tone_thr,
      trans_lumi : trans_lumi,
      bayer_range : bayer_range
    };
  }

  #InitTexture(width, height, src) {
    if (this.#texture.data != null) this.#gl.deleteTexture(this.#texture.data);
    this.#texture.data = this.#gl.createTexture();
    this.#gl.bindTexture(this.#gl.TEXTURE_2D, this.#texture.data);
    this.#gl.pixelStorei(this.#gl.UNPACK_FLIP_Y_WEBGL, true);
    this.#gl.texImage2D(this.#gl.TEXTURE_2D, 0, this.#gl.RGBA, width, height, 0, this.#gl.RGBA, this.#gl.UNSIGNED_BYTE, src);
    this.#gl.texParameteri(this.#gl.TEXTURE_2D, this.#gl.TEXTURE_MAG_FILTER, this.#gl.NEAREST);
    this.#gl.texParameteri(this.#gl.TEXTURE_2D, this.#gl.TEXTURE_MIN_FILTER, this.#gl.NEAREST);
    this.#gl.texParameteri(this.#gl.TEXTURE_2D, this.#gl.TEXTURE_WRAP_S, this.#gl.CLAMP_TO_EDGE);
    this.#gl.texParameteri(this.#gl.TEXTURE_2D, this.#gl.TEXTURE_WRAP_T, this.#gl.CLAMP_TO_EDGE);
    this.#gl.pixelStorei(this.#gl.UNPACK_FLIP_Y_WEBGL, false);
    this.#texture.width = width;
    this.#texture.height = height;
  }

  #UpdateTexture(width, height, src) {
    this.#gl.bindTexture(this.#gl.TEXTURE_2D, this.#texture.data);
    this.#gl.pixelStorei(this.#gl.UNPACK_FLIP_Y_WEBGL, true);
    this.#gl.texImage2D(this.#gl.TEXTURE_2D, 0, this.#gl.RGBA, width, height, 0, this.#gl.RGBA, this.#gl.UNSIGNED_BYTE, src);
    this.#gl.pixelStorei(this.#gl.UNPACK_FLIP_Y_WEBGL, false);
    this.#texture.width = width;
    this.#texture.height = height;
  }

  #SetDummyImage(r, g, b) {
    const dummy = new Uint8Array([r, g, b, 1.0]);
    this.#InitTexture(1, 1, dummy);
  }

  SetImage(img) {
    this.#InitTexture(img.width, img.height, img);
  }

  SetVideo(video) {
    this.#InitTexture(video.videoWidth, video.videoHeight, video);
  }

  UpdateVideo(video) {
    this.#UpdateTexture(video.videoWidth, video.videoHeight, video);
  }

  SetTransform(zoom, scroll) {
    this.#zoom = 1.0 / zoom;
    this.#scroll.x = scroll.x;
    this.#scroll.y = -scroll.y + this.#zoom - 1.0;
  } 

  Draw() {
    this.#gl.bindFramebuffer(this.#gl.FRAMEBUFFER, this.#frame_buffer.frame);
    this.#gl.viewport(0, 0, this.#frame_buffer.resolution[0], this.#frame_buffer.resolution[1]);
    this.#gl.useProgram(this.#program.program);
    this.#gl.bindVertexArray(this.#vao);
    this.#gl.activeTexture(this.#gl.TEXTURE0);
    this.#gl.bindTexture(this.#gl.TEXTURE_2D, this.#texture.data);
    this.#gl.uniform1i(this.#program.pict, 0);
    this.#gl.uniform2f(this.#program.pict_res, this.#texture.width, this.#texture.height);
    this.#gl.uniform1f(this.#program.zoom, this.#zoom);
    this.#gl.uniform2f(this.#program.scroll, this.#scroll.x, this.#scroll.y);
    const params_value = this.#params.GetValues();
    this.#gl.uniform1f(this.#program.pre_lumi_fix, params_value[0]);
    this.#gl.uniform3fv(this.#program.tone_thr, params_value[1]);
    this.#gl.uniform1f(this.#program.bayer_range, params_value[2]);
    this.#gl.uniform1f(this.#program.trans_lumi, params_value[3]);
    this.#gl.clear(this.#gl.COLOR_BUFFER_BIT | this.#gl.DEPTH_BUFFER_BIT);
    this.#gl.drawElements(this.#gl.TRIANGLES, this.#object.index_len, this.#gl.UNSIGNED_INT, 0);
    this.#screen.Draw(this.#gl, this.#frame_buffer.texture, this.#frame_buffer.resolution);
    this.#gl.flush();
  }

  GetPixelData() {
    const res = this.#frame_buffer.resolution;
    const len = res[0] * res[1] * 4;
    const buffer = new Uint8Array(len);
    this.#gl.bindFramebuffer(this.#gl.FRAMEBUFFER, this.#frame_buffer.frame);
    this.#gl.readPixels(0, 0, res[0], res[1], this.#gl.RGBA, this.#gl.UNSIGNED_BYTE, buffer);
    return buffer.filter((nouse, index) => index % 4 == 0);
  }
};