"use strict";
import { DecodePixel } from "./gbpixel_helper.mjs"

export class FrameViewer {
  #target;
  #wrap = document.createElement("div");
  #scale;
  #map_res;
  #tile_res;
  #canvas = document.createElement("canvas");
  #context;
  #palette = ["#FFFFFF", "#AAAAAA", "#555555", "#000000"];
  #frame_data = null;
  #current_frame = 0;

  constructor (div, scale, map_resolution = [20, 18], tile_resolution = [8, 8]) {
    this.#target = div;
    this.#target.appendChild(this.#wrap);
    this.#scale = scale;
    this.#map_res = map_resolution;
    this.#tile_res = tile_resolution;
    this.#canvas.style.border = "2px solid";
    this.#canvas.width = this.#map_res[0] * this.#tile_res[0] * this.#scale;
    this.#canvas.height = this.#map_res[1] * this.#tile_res[1] * this.#scale;
    this.#wrap.appendChild(this.#canvas);
    this.#context = this.#canvas.getContext("2d");
  }

  RendererFrame() {
    const tile_byte = 16;
    const frame_byte = 360;
    const map_offset = frame_byte * this.#current_frame;
    const map = new Uint8Array(this.#frame_data.map, map_offset, frame_byte);
    map.forEach((tile_number, position) => {
      const tile_offset = tile_byte * tile_number;
      const tile = this.#frame_data.tile.slice(tile_offset, tile_offset + tile_byte);
      const tile_pixel = DecodePixel(tile);
      const tile_x = position % this.#map_res[0];
      const tile_y = Math.floor(position / this.#map_res[0]);
      tile_pixel.forEach((pixel, pixel_index) => {
        const pixel_x = pixel_index % this.#tile_res[0];
        const pixel_y = Math.floor(pixel_index / this.#tile_res[0]);
        const x = (tile_x * this.#tile_res[0] + pixel_x) * this.#scale;
        const y = (tile_y * this.#tile_res[1] + pixel_y) * this.#scale;
        this.#context.fillStyle = this.#palette[pixel];
        this.#context.fillRect(x, y, this.#scale, this.#scale);
      });
    });
  }

  ClearFrame() {
    this.#context.clearRect(0, 0, this.#canvas.width, this.#canvas.height);
  }

  DrawText(str) {
    this.ClearFrame();
    this.#context.font = "20px serif";
    this.#context.fillStyle = "black";
    this.#context.fillText(str, 10, this.#canvas.height - 10);
  }

  NextFrame() {
    if (this.#frame_data == null) return;
    this.#current_frame = (this.#current_frame + 1) % this.#frame_data.frame_size;
    this.RendererFrame();
  }

  PrevFrame() {
    if (this.#frame_data == null) return;
    this.#current_frame = this.#current_frame <= 0 ? this.#frame_data.frame_size - 1 : this.#current_frame - 1;
    this.RendererFrame();
  }

  SetData(tile_tree, fps) {
    const list = tile_tree.GetTileList();
    const tile_len_max = 256;
    const tile_byte = 16;
    const tile_buffer = new ArrayBuffer(tile_len_max * tile_byte);
    const tile_array = new Uint8Array(tile_buffer);
    const frame_byte = 360;
    const map_buffer = new ArrayBuffer(tile_tree.GetFrameCount() * frame_byte);
    const map_array = new Uint8Array(map_buffer);
    const len = Math.min(tile_len_max, list.length);
    for (let i = 0; i < len; ++i) {
      const tile_data = list[i];
      const array = tile_data.uintArray;
      const write_offset = i * tile_byte;
      tile_array.set(array, write_offset);
      tile_data.frame.forEach(frame => map_array[frame.number * frame_byte + frame.position] = i & 0xFF);
    }
    this.#frame_data = {tile : tile_buffer, fps : fps, frame_size : tile_tree.GetFrameCount(), map : map_buffer};
    this.#current_frame = 0;
    this.RendererFrame();
  }

  GetData() { return this.#frame_data; }
};