"use strict";

export class LayerCanvas {
  #target;
  #wrap = document.createElement("div");
  #canvases = new Array();

  constructor (target, width, height, layer) {
    this.#target = target;
    this.#wrap.style.position = "relative";
    this.#wrap.style.display = "block";
    this.#wrap.style.width = width + "px";
    this.#wrap.style.height = height + "px";
    this.#wrap.style.border = "2px solid";
    for (let i = 0; i < layer; ++i) {
      const canvas = document.createElement("canvas");
      canvas.width = width;
      canvas.height = height;
      canvas.style.position = "absolute";
      canvas.style.left = 0;
      canvas.style.top = 0;
      canvas.style.border = "none";
      canvas.style.padding = "none";
      canvas.style.margin = "none";
      this.#wrap.appendChild(canvas);
      this.#canvases.push(canvas);
    }
    this.#target.appendChild(this.#wrap);
  }

  Resize(width, height) {
    this.#wrap.style.width = width + "px";
    this.#wrap.style.height = height + "px";
    this.#canvases.forEach(canvas => {
      canvas.width = width;
      canvas.height = height;
    });
  }

  GetWidth() { return this.#canvases[0].width; }

  GetHeight() { return this.#canvases[0].height; }

  GetContexts(id) { return this.#canvases.map(canvas => canvas.getContext(id)); }

  addEventListener(id, func) { this.#wrap.addEventListener(id, func); }

  static GetMouseCoord(event) {
    const rect = event.target.getBoundingClientRect();
    return {
      x: event.x - rect.left,
      y: event.y - rect.top
    };
  }
};