"use strict";
import { DecodePixel, EncodeTileBinary } from "./gbpixel_helper.mjs"

class TileData {
  #binary;
  #frame = new Array();

  constructor(tile_bin) { this.#binary = tile_bin; }

  static MergeTilesData(array) {
    const byte_len = array[0].arrayBuffer.byteLength * 4;
    const pixel_buffer = new Array(byte_len).fill(0);
    let frame_buffer = new Array();
    let avg_factor = 0;
    array.forEach(tile_data => {
      const pixel = tile_data.GetDecodePixels();
      frame_buffer = frame_buffer.concat(tile_data.frame);
      // for (let i = 0; i < byte_len; ++i) pixel_buffer[i] += pixel[i];
      for (let i = 0; i < byte_len; ++i) pixel_buffer[i] += pixel[i] * tile_data.frame.length;
      avg_factor += tile_data.frame.length;
    });
    for (let i = 0; i < byte_len; ++i) {
      // pixel_buffer[i] = Math.round(pixel_buffer[i] / array.length);
      pixel_buffer[i] = Math.round(pixel_buffer[i] / avg_factor);
    }
    const bin = EncodeTileBinary(pixel_buffer);
    const new_tile = new TileData(bin);
    new_tile.AddFrameData(frame_buffer);
    return new_tile;
  }

  static TilesDiff(a, b) {
    const a_bin = a.dataView;
    const b_bin = b.dataView;
    const len = Math.floor(Math.min(a.arrayBuffer.byteLength, b.arrayBuffer.byteLength) / 2);
    let accum = 0;
    for (let i = 0; i < len; ++i) {
      const a_line = a_bin.getUint16(i * 2);
      const b_line = b_bin.getUint16(i * 2);
      let diff_line = a_line ^ b_line;
      for (let j = 0; j < 8; ++j) {
        accum += (diff_line & 0x0001) != 0 ? 2 : 0;
        accum += (diff_line & 0x0100) != 0 ? 1 : 0;
        diff_line >>>= 1;
      }
    }
    return accum;
  }

  AddFrameData(frame_data) {
    if (Array.isArray(frame_data)) {
      this.#frame = this.#frame.concat(frame_data);
    } else {
      this.#frame.push(frame_data);
    }
  }

  GetDecodePixels() { return DecodePixel(this.#binary); }

  get arrayBuffer() { return this.#binary; }
  get uintArray() { return new Uint8Array(this.#binary); }
  get dataView() { return new DataView(this.#binary); }
  get frame() { return this.#frame; }
};

class TileNode {
  #value;
  #row;
  #child = new Map();

  constructor(value, row) {
    this.#value = value;
    this.#row = row;
  }

  Register(tile_data) {
    if (this.#child.has("end")) {
      const registed = this.#child.get("end");
      registed.AddFrameData(tile_data.frame)
      return registed;
    }
    const offset = (this.#row + 1) * 2;
    if (offset > tile_data.arrayBuffer.byteLength - 2) {
      this.#child.set("end", tile_data);
      return tile_data;
    }
    const value = tile_data.dataView.getUint16(offset);
    if (this.#child.has(value)) {
      return this.#child.get(value).Register(tile_data);
    } else {
      const new_node = new TileNode(value, this.#row + 1);
      this.#child.set(value, new_node);
      return new_node.Register(tile_data);
    }
  }

  Delete(tile_data) {
    const offset = (this.#row + 1) * 2;
    const byte_len = tile_data.arrayBuffer.byteLength;
    const is_leaf = offset > (byte_len - 2);
    if (is_leaf) {
      if (this.#child.has("end")) {
        return this.#child.delete("end");
      } else {
        return false;
      }
    }
    const value = tile_data.dataView.getUint16(offset);
    if (!this.#child.has(value)) return false;
    const child = this.#child.get(value);
    if (child.Delete(tile_data) && (child.size == 0)) {
      return this.#child.delete(value);
    }
    return false;
  }

  GetTileCount() {
    if (this.#child.has("end")) {
      return 1;
    } else {
      let sum = 0;
      this.#child.forEach(child => sum += child.GetTileCount());
      return sum;
    }
  }

  GetTileList() {
    if (this.#child.has("end")) {
      return this.#child.get("end");
    } else {
      let buffer = new Array();
      this.#child.forEach(child => buffer = buffer.concat(child.GetTileList()));
      return buffer;
    }
  }

  get value() { return this.#value; }
  get row() { return this.#row; }
};

function RandomIntPair(size) {
  const a = Math.floor(Math.random() * size);
  const b = Math.floor(Math.random() * size);
  if (a === b) {
    return RandomIntPair(size);
  } else {
    return [a, b];
  }
}

function GetDiffMap(tile_list) {
  const len = tile_list.length;
  const diff_list = new Map();
  for (let i = 0; i < len; ++i) {
    const a = tile_list[i];
    for (let j = i + 1; j < len; ++j) {
      const b = tile_list[j];
      const diff = TileData.TilesDiff(a, b);
      if (diff_list.has(diff)) {
        diff_list.get(diff).push([i, j]);
      } else {
        diff_list.set(diff, [[i, j]]);
      }
    }
  }
  return diff_list;
}

function GetSimilarGroup(diff_list, used_list) {
  const similar = new Array(new Set().add("dummy"));
  const list = new Set();
  diff_list.forEach(pair => {
    if (!used_list.has(pair[0]) && !used_list.has(pair[1])) {
      const index = similar.findIndex(set => set.has(pair[0]));
      if (index >= 0) {
        if (similar.findIndex(set => set.has(pair[1])) < 0) {
          similar[index].add(pair[1]);
          list.add(pair[1]);
        }
      } else {
        if (similar.findIndex(set => set.has(pair[1])) < 0) {
          similar.push(new Set().add(pair[0]).add(pair[1]));
          list.add(pair[0]).add(pair[1]);
        }
      }
    }
  });
  for(let value of list.values()) used_list.add(value);
  const array = [];
  const len = similar.length;
  for (let i = 1; i < len; ++i) array.push([...similar[i].values()]);
  return array;
}

function GetSimilarList(tile_len, diff_map) {
  const used_list = new Set();
  let similar_list = new Array;
  const diff_upper = 192;
  for (let diff = 1; diff < diff_upper; ++diff) {
    if (used_list.size >= tile_len) break;
    if (diff_map.has(diff)) {
      const tmp = GetSimilarGroup(diff_map.get(diff), used_list);
      if (tmp.length > 0) similar_list = similar_list.concat(tmp);
    }
  }
  return similar_list;
}

export class TileTree {
  #root = new TileNode (null, -1);
  #frame_count = 0;

  RegisterFrame(frame_bin) {
    const tile_bytelen = 16;
    const buffer_len = Math.floor(frame_bin.byteLength / tile_bytelen);
    for(let i = 0; i < buffer_len; ++i) {
      const head = i * tile_bytelen;
      const tile_bin = frame_bin.slice(head, head + tile_bytelen);
      const data = new TileData(tile_bin);
      data.AddFrameData({number : this.#frame_count, position : i});
      this.#root.Register(data);
    }
    ++this.#frame_count;
  }

  GetFrameCount() { return this.#frame_count; }

  GetTileCount() { return this.#root.GetTileCount(); }

  GetTileList() { return this.#root.GetTileList(); }

  #RandomReduce(size) {
    const cur_size = this.GetTileCount();
    if (cur_size <= size) return;
    const tile_list = this.GetTileList();
    const pair = RandomIntPair(cur_size - 1);
    const a = tile_list[pair[0]];
    const b = tile_list[pair[1]];
    const new_tile = TileData.MergeTilesData([a, b]);
    this.#root.Delete(a);
    this.#root.Delete(b);
    this.#root.Register(new_tile);
    this.#RandomReduce(size);
  }

  ReduceTileRnd(size) { this.#RandomReduce(size); }

  #MergeSimilarTile(target_size, tile_list, similar_list) {
    let size = tile_list.length;
    const len = similar_list.length;
    for (let i = 0; i < len; ++i) {
      if (target_size >= size) break;
      const similar = similar_list[i];
      const tiles = similar.map(index => tile_list[index]);
      const new_tile = TileData.MergeTilesData(tiles);
      tiles.forEach(tile => this.#root.Delete(tile));
      this.#root.Register(new_tile);
      size -= (tiles.length - 1);
    };
  }

  ReduceTile(size) {
    const tile_list = this.GetTileList();
    if (tile_list.length <= size) return tile_list.length;
    const diff_map = GetDiffMap(tile_list);
    const similar_list = GetSimilarList(tile_list.length, diff_map);
    this.#MergeSimilarTile(size, tile_list, similar_list);
    return this.GetTileCount();
  }

  GetFullFrame(number) {
    number %= (this.#frame_count);
    const tile_list = this.GetTileList();
    const buffer = new ArrayBuffer(360 * 16);
    const array = new Uint8Array(buffer);
    tile_list.forEach(tile_data => {
      const tile = tile_data.uintArray;
      const frames = tile_data.frame.filter(item => item.number == number);
      frames.forEach(frame => {
        const head = frame.position * 16;
        for (let i = 0; i < 16; ++i) array[head + i] = tile[i];
      });
    });
    return buffer;
  }
}