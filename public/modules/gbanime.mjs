"use strict";
import { VideoCanvas } from "./video_canvas.mjs";
import { GBImageFilter } from "./gbimagefilter.mjs";
import { TileTree } from "./tile_tree.mjs"
import { FrameViewer} from "./frame_viewer.mjs"

function DownloadBinary(data, name) {
  const blob = new Blob([data], { type : "application/octet-stream" });
  const dummy = document.createElement("a");
  dummy.href = URL.createObjectURL(blob);
  dummy.download = name;
  dummy.click();
  URL.revokeObjectURL(dummy.href);
}

function SetGlobalChecksum(rom_array) {
  const checksum_address = 0x014E;
  const checksum_length = 2;
  const head1 = 0;
  const tail1 = checksum_address;
  const head2 = tail1 + checksum_length;
  const tail2 = rom_array.length;
  let accum = 0;
  for (let i = head1; i < tail1; ++i) { accum += rom_array[i]; }
  for (let i = head2; i < tail2; ++i) { accum += rom_array[i]; }
  rom_array[checksum_address + 0] = (accum >>> 8) & 0xFF;
  rom_array[checksum_address + 1] = accum & 0xFF;
}

export class GBAnime {
  static #resolution = [160, 144];
  #div = null;
  #wrap = document.createElement("div");
  #elem = {
    file : document.createElement("input"),
    play : document.createElement("button"),
    fps  : document.createElement("select"),
    len  : document.createElement("input"),
    pos  : document.createElement("input"),
    prev : document.createElement("button"),
    next : document.createElement("button"),
    conv : document.createElement("button"),
    rom  : document.createElement("button"),
    bin  : document.createElement("button")
  };
  #preview = null;
  #filter = null;
  #frame = null;
  #lock = false;
  
  constructor(div, init_video = null, scale = 2) {
    this.#div = div;
    this.#wrap.style.display = "flex";
    this.#elem.file.type = "file";
    this.#elem.file.accept = "video/*";
    this.#elem.file.addEventListener("input", () => this.#InputFileCallback());
    this.#div.appendChild(this.#elem.file);
    this.#div.appendChild(document.createElement("br"));
    this.#elem.play.style.width = "50px";
    this.#elem.play.innerText = "play";
    this.#elem.play.addEventListener("click", () => this.#PlayButtonCallback());
    this.#div.appendChild(this.#elem.play);
    this.#InitFPSSelect();
    this.#div.appendChild(this.#elem.fps);
    this.#elem.len.type = "number";
    this.#elem.len.min = 1;
    this.#elem.len.max = 60;
    this.#elem.len.value = 60;
    this.#elem.len.addEventListener("change", () => this.#LengthCallback());
    this.#div.appendChild(this.#elem.len);
    this.#elem.pos.type = "range";
    this.#elem.pos.min = 0.0;
    this.#elem.pos.max = 1.0;
    this.#elem.pos.step = 0.0000000001;
    this.#elem.pos.value = 0.0;
    this.#elem.pos.addEventListener("change", () => this.#PositionCallback());
    this.#div.appendChild(this.#elem.pos);
    this.#div.appendChild(document.createElement("br"));
    this.#div.appendChild(this.#wrap);
    this.#preview = new VideoCanvas(this.#wrap, [GBAnime.#resolution[0] * scale, GBAnime.#resolution[1] * scale], 60, init_video);
    this.#filter = new GBImageFilter(this.#wrap, GBAnime.#resolution, scale);
    this.#preview.SetLoadVideoCallback(video => this.#filter.UpdateVideo(video));
    this.#preview.SetOperationCallback((zoom, scroll) => this.#filter.SetTransform(zoom, scroll));
    this.#preview.SetDrawCallback(() => this.#filter.Draw());
    this.#elem.prev.innerText = "<<";
    this.#elem.prev.addEventListener("click", () => this.#frame.PrevFrame());
    this.#div.appendChild(this.#elem.prev);
    this.#elem.next.innerText = ">>";
    this.#elem.next.addEventListener("click", () => this.#frame.NextFrame());
    this.#div.appendChild(this.#elem.next);
    this.#elem.conv.innerText = "convert";
    this.#elem.conv.addEventListener("click", () => this.#Convert());
    this.#div.appendChild(this.#elem.conv);
    this.#elem.rom.innerText = "save rom";
    this.#elem.rom.addEventListener("click", () => this.#SaveRom());
    this.#div.appendChild(this.#elem.rom);
    this.#elem.bin.innerText = "save binary";
    this.#elem.bin.addEventListener("click", () => this.#SaveAnimeBinary());
    this.#div.appendChild(this.#elem.bin);
    this.#div.appendChild(document.createElement("br"));
    const tmp = document.createElement("div");
    this.#div.appendChild(tmp);
    this.#frame = new FrameViewer(tmp, scale);
  }

  #InitFPSSelect() {
    const base = 60;
    for (let i = 1; i <= 60; ++i) {
      const num = base / i;
      if (Number.isInteger(num)) {
        const opt = document.createElement("option");
        opt.value = num ;
        opt.innerText = num ;
        this.#elem.fps.appendChild(opt);
      }
    }
    this.#elem.fps.addEventListener("change", () => this.#FpsCallback());
  }

  #InputFileCallback() {
    if (this.#lock) return;
    if (this.#elem.file.files.length == 0) return;
    if (this.#preview.IsPlay()) this.#PlayButtonCallback();
    const reader = new FileReader();
    reader.onload = (event) => this.#preview.SetVideoSource(event.target.result);
    reader.readAsDataURL(this.#elem.file.files[0]);
  }

  #PlayButtonCallback() {
    if (this.#lock) return;
    if (this.#preview.IsPlay()) {
      this.#preview.Pause();
      this.#elem.play.innerText = "play";
    } else {
      this.#preview.Play();
      this.#elem.play.innerText = "pause";
    }
  }

  #FpsCallback() {
    if (this.#lock) return;
    this.#preview.SetFPS(this.#elem.fps.value)
  }

  #LengthCallback() {
    if (this.#lock) return;
    this.#elem.len.value = Math.max(1, this.#elem.len.value);
    this.#elem.len.value = Math.min(60, this.#elem.len.value);
    this.#preview.SetLength(this.#elem.len.value);
  }

  #PositionCallback() {
    if (this.#lock) return;
    this.#preview.SetPosition(this.#elem.pos.value);
  }

  #Lock() {
    this.#lock = true;
    for (const key in this.#elem) {
      this.#elem[key].style.display = "none";
    }
  }

  #Unlock() {
    this.#lock = false;
    for (const key in this.#elem) {
      this.#elem[key].style.display = "";
    }
  }

  #GetAnimeBinary() {
    const data = this.#frame.GetData();
    if (data == null) return null;
    const frame_div = 60 / data.fps;
    const buffer = new ArrayBuffer(data.tile.byteLength + 1 + 1 + data.map.byteLength); 
    const array = new Uint8Array(buffer);
    array.set(new Uint8Array(data.tile), 0);
    array[data.tile.byteLength] = frame_div;
    array[data.tile.byteLength + 1] = data.frame_size;
    array.set(new Uint8Array(data.map), data.tile.byteLength + 1 + 1);
    return buffer;
  }

  async #GetBaseRomBinary() {
    const response = await fetch("./resources/rom.gb");
    return await response.arrayBuffer();
  }

  async #SaveRom() {
    if (this.#lock) return;
    const bin = this.#GetAnimeBinary();
    if (bin == null) return;
    const rom = await this.#GetBaseRomBinary();
    const rom_array = new Uint8Array(rom);
    const head = 0x150;
    rom_array.set(new Uint8Array(bin), head);
    SetGlobalChecksum(rom_array);
    DownloadBinary(rom, "gbanime.gb")
  }

  #SaveAnimeBinary() {
    if (this.#lock) return;
    const bin = this.#GetAnimeBinary();
    if (bin == null) return;
    DownloadBinary(bin, "animedata.bin");
  }

  async #Convert() {
    if (this.#lock) return;
    this.#Lock();
    const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
    const wait = 250;
    const tree = new TileTree();
    if (this.#preview.IsPlay()) this.#PlayButtonCallback();
    await this.#preview.Stop(wait);
    const len = this.#elem.len.value;
    for (let i = 0; i < len; ++i) {
      this.#frame.DrawText("draw " + i + " / " + len);
      await sleep(wait);
      const bin = this.GetFrameTileData();
      tree.RegisterFrame(bin);
      const adjust_thr = 10000;
      if (!await this.#preview.NextFrame(wait)) break;
      const count = tree.GetTileCount();
      if (count > adjust_thr) {
        this.#frame.DrawText("reduce " + count + " -> " + Math.floor(count / 2));
        await sleep(wait);
        tree.ReduceTile(Math.floor(count / 2));
      }
    }
    const tgt_size = 256;
    this.#frame.DrawText("reduce " + tree.GetTileCount() + " -> " + tgt_size);
    await sleep(wait);
    while(true) {
      const size = tree.ReduceTile(tgt_size);
      if (size <= tgt_size) {
        break;
      } else {
        this.#frame.DrawText("reduce " + size + " -> " + tgt_size);
        await sleep(wait);
      }
    }
    tree.ReduceTile(tgt_size);
    this.#frame.SetData(tree, this.#preview.GetFPS());
    this.#Unlock();
  }

  GetFrameTilePixel() {
    const raw = this.#filter.GetPixelData();
    const tile_x = 160 / 8;
    const tile_y = 144 / 8;
    const tile_len = tile_x * tile_y;
    const buffer = new Uint8Array(raw.length);
    let counter = 0;
    for (let tile = 0; tile < tile_len; ++tile) {
      const head_x = (tile * 8) % 160;
      const head_y = Math.floor(tile / tile_x) * 8;
      for (let pixel = 0; pixel < 64; ++pixel) {
        const pixel_x = head_x + (pixel % 8);
        const pixel_y = head_y + Math.floor(pixel / 8);
        const index = pixel_x + (144 - 1 - pixel_y) * 160;
        buffer[counter++] = raw[index] < 255 / 3 * 1 ? 3 :
                            raw[index] < 255 / 3 * 2 ? 2 :
                            raw[index] < 255 / 3 * 3 ? 1 :
                            0;
      }
    }
    return buffer;
  }

  GetFrameTileData() {
    const data = this.GetFrameTilePixel();
    const bit_len = 8;
    const len = data.length;
    const buffer = new ArrayBuffer(Math.ceil(len / 4));
    const array = new Uint8Array(buffer);
    let index = 0;
    let pixel = 0;
    let lo_buffer = 0;
    let hi_buffer = 0;
    for (let i = 0; i < len; ++i) {
      const bit = (bit_len - 1) - pixel;
      const lo = data[i] & 1;
      const hi = (data[i] & 2) >>> 1;
      lo_buffer |= lo << bit;
      hi_buffer |= hi << bit;
      if (++pixel >= bit_len) {
        pixel = 0;
        array[index++] = lo_buffer;
        array[index++] = hi_buffer;
        lo_buffer = 0;
        hi_buffer = 0;
      }
    }
    if (pixel != 0) {
      array[index++] = lo_buffer;
      array[index++] = hi_buffer;
    }
    return buffer;
  }

};