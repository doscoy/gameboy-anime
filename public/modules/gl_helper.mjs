"use strict";

function Clamp(n, min, max) {
  return Math.min(Math.max(n, min), max);
}

export function CreateGLContext(canvas) {
  return canvas.getContext("webgl2");
};

function CreateShader(gl, source, type) {
  let shader = gl.createShader(type);
  gl.shaderSource(shader, source);
  gl.compileShader(shader);
  if (gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    return shader;
  } else {
    console.log("shader error", gl.getShaderInfoLog(shader), source);
  }
}

export function CreateVertexShader(gl, source) {
  return CreateShader(gl, source, gl.VERTEX_SHADER);
}

export function CreateFragmentShader(gl, source) {
  return CreateShader(gl, source, gl.FRAGMENT_SHADER);
}

export function CreateProgram(gl, vertex, fragment) {
  let program = gl.createProgram();
  gl.attachShader(program, vertex);
  gl.attachShader(program, fragment);
  gl.linkProgram(program);
  if (gl.getProgramParameter(program, gl.LINK_STATUS)) {
    return program;
  } else {
    console.log("program error", gl.getProgramInfoLog(program));
  }
}

export function CreateAttributeRecipe(name, length, type) {
  let tmp = new Object();
  if (Array.isArray(name) && Array.isArray(length) && Array.isArray(type)) {
    let len = Math.min(name.length, length.length, type.length);
    for (let i = 0; i < len; ++i) {
      tmp[name[i]] = {"length" : length[i], "type" : type[i]};
    }
    return tmp;
  } else {
    return [{"name" : name, "length" : length, "type" : type}]; 
  }
}

function CreatePanel(origin, size) {
  return {
    "vertices": new Float32Array([
      origin[0],           origin[1],           0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
      origin[0] + size[0], origin[1],           0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0,
      origin[0] + size[0], origin[1] + size[1], 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0,
      origin[0],           origin[1] + size[1], 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0
    ]),
    "indices": new Int32Array([
      0, 1, 2,
      0, 2, 3
    ]),
    "attribute" : CreateAttributeRecipe(["vp", "vn", "vc"], [3, 3, 3], ["vec3", "vec3", "vec3"]),
    "strides": [3, 3, 3]
  };
}

function HSV2RGB(h, s, v) {
  s = Clamp(s, 0.0, 1.0);
  v = Clamp(v, 0.0, 1.0);
  let th = h % 360;
  let i = Math.floor(th / 60);
  let f = th / 60 - i;
  let m = v * (1 - s);
  let n = v * (1 - s * f);
  let k = v * (1 - s * (1 - f));
  if (!s > 0 && !s < 0) {
    return {
      "r": v,
      "g": v,
      "b": v
    };
  } else {
    let r = new Array(v, n, m, m, k, v);
    let g = new Array(k, v, v, n, m, m);
    let b = new Array(m, m, k, v, v, n);
    return {
      "r": r[i],
      "g": g[i],
      "b": b[i]
    };
  }
}

function CreateTorus(row, column, irad, orad) {
  let vertices = new Array();
  let indices = new Array();
  for (let i = 0; i <= row; ++i) {
    let r = Math.PI * 2 / row * i;
    let rr = Math.cos(r);
    let ry = Math.sin(r);
    for (let j = 0; j <= column; ++j) {
      let tr = Math.PI * 2 / column * j;
      let tx = (rr * irad + orad) * Math.cos(tr);
      let ty = ry * irad;
      let tz = (rr * irad + orad) * Math.sin(tr);
      let rx = rr * Math.cos(tr);
      let rz = rr * Math.sin(tr);
      let rgba = HSV2RGB(360 / column * j, 1, 1, 1);
      vertices.push(tx, ty, tz, rx, ry, rz, rgba.r, rgba.g, rgba.b);
    }
  }
  for (let i = 0; i < row; ++i) {
    for (let j = 0; j < column; ++j) {
      let r = (column + 1) * i + j;
      indices.push(r, r + column + 1, r + 1);
      indices.push(r + column + 1, r + column + 2, r + 1);
    }
  }
  return {
    "vertices": new Float32Array(vertices),
    "indices": new Int32Array(indices),
    "attribute" : CreateAttributeRecipe(["vp", "vn", "vc"],[3, 3, 3],["vec3", "vec3", "vec3"]),
    "strides": [3, 3, 3]
  };
}

function ParseWavefrontRawData(wavefront) {
  let raw_data = wavefront.split('\n');
  let raw_v = new Array();
  let raw_vt = new Array();
  let raw_vn = new Array();
  let raw_f = new Array();
  let box = {
    "x": { "min": 0, "max": 0, "center": 0 },
    "y": { "min": 0, "max": 0, "center": 0 },
    "z": { "min": 0, "max": 0, "center": 0 },
    "size": 0
  }
  raw_data.forEach(item => {
    let tmp = item.trim().replace(/ {2,}/, " ").split(' ');
    switch (tmp[0]) {
      case "v":
        box.x.min = Math.min(box.x.min, tmp[1]);
        box.x.max = Math.max(box.x.max, tmp[1]);
        box.y.min = Math.min(box.y.min, tmp[2]);
        box.y.max = Math.max(box.y.max, tmp[2]);
        box.z.min = Math.min(box.z.min, tmp[3]);
        box.z.max = Math.max(box.z.max, tmp[3]);
        raw_v.push([tmp[1], tmp[2], tmp[3]]);
        break;
      case "vt":
        if (tmp.length == 3) {
          raw_vt.push([tmp[1], tmp[2], 0.0]);
        } else {
          raw_vt.push([tmp[1], tmp[2], tmp[3]]);
        }
        break;
      case "vn":
        raw_vn.push([tmp[1], tmp[2], tmp[3]]);
        break;
      case "f":
        for (let i = 2; i < tmp.length - 1; ++i) {
          let fix_func = n => { return n - 1; };
          raw_f.push([
            tmp[1].split('/').map(fix_func),
            tmp[i].split('/').map(fix_func),
            tmp[i + 1].split('/').map(fix_func)]);
        }
        break;
    }
  });
  let size_x = box.x.max - box.x.min;
  let size_y = box.y.max - box.y.min;
  let size_z = box.z.max - box.z.min;
  box.x.center = size_x * 0.5 + box.x.min;
  box.y.center = size_y * 0.5 + box.y.min;
  box.z.center = size_z * 0.5 + box.z.min;
  box.size = Math.max(size_x, size_y, size_z);
  return {
    "v": raw_v,
    "vt": raw_vt,
    "vn": raw_vn,
    "f": raw_f,
    "box": box
  };
}

function SetNormal(data) {
  let stride = data.strides.reduce((accum, n) => accum + n);
  let duplication = new Set();
  for (let i = 0; i < data.indices.length; i += 3) {
    let vert_a = [
      data.vertices[data.indices[i] * stride + 0],
      data.vertices[data.indices[i] * stride + 1],
      data.vertices[data.indices[i] * stride + 2]
    ];
    let vert_b = [
      data.vertices[data.indices[i + 1] * stride + 0],
      data.vertices[data.indices[i + 1] * stride + 1],
      data.vertices[data.indices[i + 1] * stride + 2]
    ];
    let vert_c = [
      data.vertices[data.indices[i + 2] * stride + 0],
      data.vertices[data.indices[i + 2] * stride + 1],
      data.vertices[data.indices[i + 2] * stride + 2]
    ];
    let vec_a = vert_a.map((a, index) => vert_b[index] - a);
    let vec_b = vert_a.map((a, index) => vert_c[index] - a);
    let norm = NormalizeVec3(CrossVec3(vec_a, vec_b));
    for (let j = 0; j < 3; ++j) {
      let index = data.indices[i + j];
      if (duplication.has(index)) {
        for (let k = 0; k < 3; ++k) {
          let ave = (norm[k] + data.vertices[index * stride + data.strides[0] + k]) * 0.5;
          data.vertices[index * stride + data.strides[0] + k] = ave;
        }
      } else {
        for (let k = 0; k < 3; ++k) {
          data.vertices[index * stride + data.strides[0] + k] = norm[k];
        }
        duplication.add(index);
      }
    }
  }
  return data;
}

function CreateWavefront(path) {
  let req = new XMLHttpRequest();
  req.open("GET", path, true);
  req.send(null);
  return new Promise(resolve => {
    req.onload = () => {
      if (req.status == 200) {
        let raw_data = ParseWavefrontRawData(req.responseText);
        let vertices = new Array();
        let indices = new Array();
        let vertices_map = new Map();
        let counter = 0;
        let set_normal_flag = false;
        raw_data.f.forEach(items => {
          items.forEach(item => {
            if (vertices_map.has(item)) {
              indices.push(vertices_map.get(item));
            } else {
              if (item.length > 1) {
                vertices.push(
                  (raw_data.v[item[0]][0] - raw_data.box.x.center) / raw_data.box.size,  // v.x
                  (raw_data.v[item[0]][1] - raw_data.box.y.min) / raw_data.box.size,  // v.y
                  (raw_data.v[item[0]][2] - raw_data.box.z.center) / raw_data.box.size,  // v.z
                  item[2] != -1 ? raw_data.vn[item[2]][0] : 0.0,   // vn.x
                  item[2] != -1 ? raw_data.vn[item[2]][1] : 0.0,   // vn.y
                  item[2] != -1 ? raw_data.vn[item[2]][2] : 0.0,   // vn.z
                  item[1] != -1 ? raw_data.vt[item[1]][0] : 0.0,   // r(vt.x)
                  item[1] != -1 ? raw_data.vt[item[1]][1] : 0.0,   // g(vt.y)
                  item[1] != -1 ? raw_data.vt[item[1]][2] : 0.0,   // b(vt.z?)
                );
              } else {
                set_normal_flag = true;
                vertices.push(
                  (raw_data.v[item[0]][0] - raw_data.box.x.center) / raw_data.box.size,  // v.x
                  (raw_data.v[item[0]][1] - raw_data.box.y.min) / raw_data.box.size,  // v.y
                  (raw_data.v[item[0]][2] - raw_data.box.z.center) / raw_data.box.size,  // v.z
                  0.0,                                             // vn.x
                  0.0,                                             // vn.y
                  0.0,                                             // vn.z
                  0.0,                                             // r(vt.x)
                  0.0,                                             // g(vt.y)
                  0.0,                                             // b(vt.z?)
                );
              }
              indices.push(counter);
              vertices_map.set(item, counter);
              counter++;
            }
          });
        });
        let data = {
          "vertices" : new Float32Array(vertices),
          "indices" : new Int32Array(indices),
          "attribute" : CreateAttributeRecipe(["vp", "vn", "vc"], [3, 3, 3], ["vec3", "vec3", "vec3"]),
          "strides": [3, 3, 3]
        };
        if (set_normal_flag) {
          data = SetNormal(data);
        }
        resolve(data);
      } else {
        resolve(CreatePanel([-1.0, -1.0], [2.0, 2.0]));
      }
    };
  });
}

function CreateObject(gl, layout, data) {
  let elem_bytes = data.vertices.BYTES_PER_ELEMENT;
  let attrib_arr = Object.entries(data.attribute);
  let stride = elem_bytes * attrib_arr.reduce((acumm, item) => acumm + item[1].length, 0);
  let vbo = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
  gl.bufferData(gl.ARRAY_BUFFER, data.vertices, gl.STATIC_DRAW);
  let accum = 0;
  let layout_num = 0;
  for (let i = 0; i < attrib_arr.length; ++i) {
    layout_num = i + layout;
    let offset = elem_bytes * accum;
    gl.vertexAttribPointer(layout_num, attrib_arr[i][1].length, gl.FLOAT, false, stride, offset);
    gl.enableVertexAttribArray(layout_num);
    data.attribute[attrib_arr[i][0]].layout = layout_num;
    data.attribute[attrib_arr[i][0]].offset = offset;
    accum += attrib_arr[i][1].length;
  }
  let ebo = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebo);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, data.indices, gl.STATIC_DRAW);
  return {
    "vbo": vbo,
    "ebo": ebo,
    "attribute" : data.attribute,
    "stride" : stride,
    "max_layout_num" : layout_num,
    "buffer_len" : data.vertices.length,
    "index_len": data.indices.length
  }
}

export function CreatePanelObject(gl, origin = [-1.0, -1.0], size = [2.0, 2.0], layout = 0) {
  return CreateObject(gl, layout, CreatePanel(origin, size));
}

export function CreateTorusObject(gl, row, column, irad, orad, layout = 0) {
  return CreateObject(gl, layout, CreateTorus(row, column, irad, orad));
}

export async function CreateWavefrontObject(gl, path, layout = 0) {
  return CreateWavefront(path).then(data => CreateObject(gl, layout, data));
}

export function DeleteVAO(gl, vao) {
  gl.deleteVertexArray(vao);
}

export function DeleteObject(gl, object) {
  gl.deleteBuffer(object.vbo);
  gl.deleteBuffer(object.ebo);
}

export function CreateInstanceBufferInterlive(gl, layout, recipe, size, divisor) {
  let recipe_arr = Object.entries(recipe);
  let recipe_size = recipe_arr.reduce((acumm, item) => acumm + item[1].length, 0);
  let buffer = new Float32Array(recipe_size * size);
  let elem_bytes = buffer.BYTES_PER_ELEMENT;
  let stride = elem_bytes * recipe_size;
  let vbo = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
  gl.bufferData(gl.ARRAY_BUFFER, buffer, gl.STREAM_DRAW);
  let accum = 0;
  let layout_num = 0;
  for (let i = 0; i < recipe_arr.length; ++i) {
    layout_num = i + layout;
    let offset = elem_bytes * accum;
    gl.vertexAttribPointer(layout_num, recipe_arr[i][1].length, gl.FLOAT, false, stride, offset);
    gl.enableVertexAttribArray(layout_num);
    gl.vertexAttribDivisor(layout_num, divisor);
    recipe[recipe_arr[i][0]].layout = layout_num;
    recipe[recipe_arr[i][0]].offset = offset;
    accum += recipe_arr[i][1].length;
  }
  return {
    "vbo" : vbo,
    "attribute" : recipe,
    "stride" : stride,
    "divisor" : divisor,
    "max_layout_num" : layout_num,
    "buffer_len" : buffer.length
  }
}

export function CreateInstanceBuffer(gl, layout, recipe, size, divisor) {
  let recipe_arr = Object.entries(recipe);
  let recipe_size = recipe_arr.reduce((acumm, item) => acumm + item[1].length, 0);
  let buffer = new Float32Array(recipe_size * size);
  let elem_bytes = buffer.BYTES_PER_ELEMENT;
  let vbo = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
  gl.bufferData(gl.ARRAY_BUFFER, buffer, gl.STREAM_DRAW);
  let offset = 0;
  let layout_num = 0;
  for (let i = 0; i < recipe_arr.length; ++i) {
    layout_num = i + layout;
    gl.vertexAttribPointer(layout_num, recipe_arr[i][1].length, gl.FLOAT, false, 0, offset);
    gl.enableVertexAttribArray(layout_num);
    gl.vertexAttribDivisor(layout_num, divisor);
    recipe[recipe_arr[i][0]].layout = layout_num;
    recipe[recipe_arr[i][0]].offset = offset;
    offset += elem_bytes * recipe_arr[i][1].length * size;
  }
  return {
    "vbo" : vbo,
    "attribute" : recipe,
    "divisor" : divisor,
    "max_layout_num" : layout_num,
    "buffer_len" : buffer.length
  }
}

function CreateMatrix(row, col) {
  let col_byte = Float32Array.BYTES_PER_ELEMENT * row;
  let buffer = new ArrayBuffer(col_byte * col);
  let raw = new Float32Array(buffer, 0, row * col);
  let matrix = [];
  for (let c = 0; c < col; ++c) {
    let tmp = new Float32Array(buffer, c * col_byte, col);
    matrix.push(tmp);
  }
  return {
    "mat": matrix,
    "raw": raw
  }
}

export function CreateMatrix4x4() {
  let matrix = CreateMatrix(4, 4);
  matrix.mat[0][0] = 1.0;
  matrix.mat[0][1] = 0.0;
  matrix.mat[0][2] = 0.0;
  matrix.mat[0][3] = 0.0;
  matrix.mat[1][0] = 0.0;
  matrix.mat[1][1] = 1.0;
  matrix.mat[1][2] = 0.0;
  matrix.mat[1][3] = 0.0;
  matrix.mat[2][0] = 0.0;
  matrix.mat[2][1] = 0.0;
  matrix.mat[2][2] = 1.0;
  matrix.mat[2][3] = 0.0;
  matrix.mat[3][0] = 0.0;
  matrix.mat[3][1] = 0.0;
  matrix.mat[3][2] = 0.0;
  matrix.mat[3][3] = 1.0;
  return matrix;
}

function ForceKillAssert(bool, message) {
  if (!bool) {
    console.error(message);
    ThisIsNonExistentFunctionAndCallThisWillStopTheScript();
  }
}

export function MultiplyMatrix(...matrices) {
  let len = matrices.length;
  let lhs = matrices[0];
  let tmp = null;
  for (let i = 1; i < len; ++i) {
    let rhs = matrices[i];
    ForceKillAssert(lhs.mat.length == rhs.mat[0].length, "can't multiply");
    let factor = lhs.mat.length;
    let row = lhs.mat[0].length;
    let col = rhs.mat.length;
    tmp = CreateMatrix(row, col);
    for (let j = 0; j < col; ++j) {
      for (let i = 0; i < row; ++i) {
        for (let k = 0; k < factor; ++k) {
          tmp.mat[j][i] += lhs.mat[k][i] * rhs.mat[j][k];
        }
      }
    }
    lhs = tmp;
  }
  return tmp;
}

function CrossVec3(x, y) {
  return [x[1] * y[2] - y[1] * x[2],
  x[2] * y[0] - y[2] * x[0],
  x[0] * y[1] - y[0] * x[1]];
}

function LengthVec3(vec) {
  return Math.sqrt(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2]);
}

function NormalizeVec3(vec) {
  let len = LengthVec3(vec);
  if (len > 0.00001) {
    return vec.map((value) => value / len);
  } else {
    return [0.0, 0.0, 0.0];
  }
}

export function CreateTranslateMatrix(vec) {
  let tmp = CreateMatrix4x4();
  tmp.mat[3][0] = vec[0];
  tmp.mat[3][1] = vec[1];
  tmp.mat[3][2] = vec[2];
  return tmp;
}

export function CreateRotateMatrix(axis, deg) {
  let tmp = CreateMatrix4x4();
  let len = LengthVec3(axis);
  if (len == 0.0 || deg == 0.0) {
    return tmp;
  }
  let n_axis = NormalizeVec3(axis);
  let rad = deg * (Math.PI / 180);
  let cos = Math.cos(rad);
  let inv_cos = 1.0 - cos;
  let sin = Math.sin(rad);
  tmp.mat[0][0] = (n_axis[0] * n_axis[0]) * inv_cos + cos;
  tmp.mat[1][0] = (n_axis[0] * n_axis[1]) * inv_cos - n_axis[2] * sin;
  tmp.mat[2][0] = (n_axis[0] * n_axis[2]) * inv_cos + n_axis[1] * sin;
  tmp.mat[0][1] = (n_axis[1] * n_axis[0]) * inv_cos + n_axis[2] * sin;
  tmp.mat[1][1] = (n_axis[1] * n_axis[1]) * inv_cos + cos;
  tmp.mat[2][1] = (n_axis[1] * n_axis[2]) * inv_cos - n_axis[0] * sin;
  tmp.mat[0][2] = (n_axis[0] * n_axis[2]) * inv_cos - n_axis[1] * sin;
  tmp.mat[1][2] = (n_axis[1] * n_axis[2]) * inv_cos + n_axis[0] * sin;
  tmp.mat[2][2] = (n_axis[2] * n_axis[2]) * inv_cos + cos;
  return tmp;
}

export function CreateLookAtMatrix(eye, center, up) {
  let tv = CreateTranslateMatrix([-eye[0], -eye[1], -eye[2]]);
  let t = eye.map((nouse, index) => eye[index] - center[index]);
  let r = CrossVec3(up, t);
  let s = CrossVec3(t, r);
  if (LengthVec3(s) == 0.0) {
    return tv;
  }
  t = NormalizeVec3(t);
  r = NormalizeVec3(r);
  s = NormalizeVec3(s);
  let tmp = CreateMatrix4x4();
  tmp.mat[0][0] = r[0];
  tmp.mat[1][0] = r[1];
  tmp.mat[2][0] = r[2];
  tmp.mat[0][1] = s[0];
  tmp.mat[1][1] = s[1];
  tmp.mat[2][1] = s[2];
  tmp.mat[0][2] = t[0];
  tmp.mat[1][2] = t[1];
  tmp.mat[2][2] = t[2];
  return MultiplyMatrix(tmp, tv);
}

export function CreatePerspectiveMatrix(y_fov, resolution, z_near, z_far) {
  let aspect = resolution[0] / resolution[1];
  let dz = z_far - z_near;
  let tmp = CreateMatrix4x4();
  if (dz != 0.0) {
    let f = 1.0 / Math.tan(y_fov * Math.PI / 360.0);
    tmp.mat[0][0] = f / aspect;
    tmp.mat[1][1] = f;
    tmp.mat[2][2] = -((z_far + z_near) / dz);
    tmp.mat[3][2] = -((2.0 * z_far * z_near) / dz);
    tmp.mat[2][3] = -1.0;
    tmp.mat[3][3] = 0.0;
  }
  return tmp;
}

export function CreateInverseMatrix4x4(matrix) {
  let tmp = CreateMatrix(4, 4);
  let a00a11_a01a10 = matrix.mat[0][0] * matrix.mat[1][1] - matrix.mat[0][1] * matrix.mat[1][0];
  let a00a12_a02a10 = matrix.mat[0][0] * matrix.mat[1][2] - matrix.mat[0][2] * matrix.mat[1][0];
  let a00a13_a03a10 = matrix.mat[0][0] * matrix.mat[1][3] - matrix.mat[0][3] * matrix.mat[1][0];
  let a01a12_a02a11 = matrix.mat[0][1] * matrix.mat[1][2] - matrix.mat[0][2] * matrix.mat[1][1];
  let a01a13_a03a11 = matrix.mat[0][1] * matrix.mat[1][3] - matrix.mat[0][3] * matrix.mat[1][1];
  let a02a13_a03a12 = matrix.mat[0][2] * matrix.mat[1][3] - matrix.mat[0][3] * matrix.mat[1][2];
  let a20a31_a21a30 = matrix.mat[2][0] * matrix.mat[3][1] - matrix.mat[2][1] * matrix.mat[3][0];
  let a20a32_a22a30 = matrix.mat[2][0] * matrix.mat[3][2] - matrix.mat[2][2] * matrix.mat[3][0];
  let a20a33_a23a30 = matrix.mat[2][0] * matrix.mat[3][3] - matrix.mat[2][3] * matrix.mat[3][0];
  let a21a32_a22a31 = matrix.mat[2][1] * matrix.mat[3][2] - matrix.mat[2][2] * matrix.mat[3][1];
  let a21a33_a23a31 = matrix.mat[2][1] * matrix.mat[3][3] - matrix.mat[2][3] * matrix.mat[3][1];
  let a22a33_a23a32 = matrix.mat[2][2] * matrix.mat[3][3] - matrix.mat[2][3] * matrix.mat[3][2];
  let inv_det = 1.0 / (a00a11_a01a10 * a22a33_a23a32 -
    a00a12_a02a10 * a21a33_a23a31 +
    a00a13_a03a10 * a21a32_a22a31 +
    a01a12_a02a11 * a20a33_a23a30 -
    a01a13_a03a11 * a20a32_a22a30 +
    a02a13_a03a12 * a20a31_a21a30);
  tmp.mat[0][0] = (matrix.mat[1][1] * a22a33_a23a32 -
    matrix.mat[1][2] * a21a33_a23a31 +
    matrix.mat[1][3] * a21a32_a22a31) * inv_det;
  tmp.mat[0][1] = (-matrix.mat[0][1] * a22a33_a23a32 +
    matrix.mat[0][2] * a21a33_a23a31 -
    matrix.mat[0][3] * a21a32_a22a31) * inv_det;
  tmp.mat[0][2] = (matrix.mat[3][1] * a02a13_a03a12 -
    matrix.mat[3][2] * a01a13_a03a11 +
    matrix.mat[3][3] * a01a12_a02a11) * inv_det;
  tmp.mat[0][3] = (-matrix.mat[2][1] * a02a13_a03a12 +
    matrix.mat[2][2] * a01a13_a03a11 -
    matrix.mat[2][3] * a01a12_a02a11) * inv_det;
  tmp.mat[1][0] = (-matrix.mat[1][0] * a22a33_a23a32 +
    matrix.mat[1][2] * a20a33_a23a30 -
    matrix.mat[1][3] * a20a32_a22a30) * inv_det;
  tmp.mat[1][1] = (matrix.mat[0][0] * a22a33_a23a32 -
    matrix.mat[0][2] * a20a33_a23a30 +
    matrix.mat[0][3] * a20a32_a22a30) * inv_det;
  tmp.mat[1][2] = (-matrix.mat[3][0] * a02a13_a03a12 +
    matrix.mat[3][2] * a00a13_a03a10 -
    matrix.mat[3][3] * a00a12_a02a10) * inv_det;
  tmp.mat[1][3] = (matrix.mat[2][0] * a02a13_a03a12 -
    matrix.mat[2][2] * a00a13_a03a10 +
    matrix.mat[2][3] * a00a12_a02a10) * inv_det;
  tmp.mat[2][0] = (matrix.mat[1][0] * a21a33_a23a31 -
    matrix.mat[1][1] * a20a33_a23a30 +
    matrix.mat[1][3] * a20a31_a21a30) * inv_det;
  tmp.mat[2][1] = (-matrix.mat[0][0] * a21a33_a23a31 +
    matrix.mat[0][1] * a20a33_a23a30 -
    matrix.mat[0][3] * a20a31_a21a30) * inv_det;
  tmp.mat[2][2] = (matrix.mat[3][0] * a01a13_a03a11 -
    matrix.mat[3][1] * a00a13_a03a10 +
    matrix.mat[3][3] * a00a11_a01a10) * inv_det;
  tmp.mat[2][3] = (-matrix.mat[2][0] * a01a13_a03a11 +
    matrix.mat[2][1] * a00a13_a03a10 -
    matrix.mat[2][3] * a00a11_a01a10) * inv_det;
  tmp.mat[3][0] = (-matrix.mat[1][0] * a21a32_a22a31 +
    matrix.mat[1][1] * a20a32_a22a30 -
    matrix.mat[1][2] * a20a31_a21a30) * inv_det;
  tmp.mat[3][1] = (matrix.mat[0][0] * a21a32_a22a31 -
    matrix.mat[0][1] * a20a32_a22a30 +
    matrix.mat[0][2] * a20a31_a21a30) * inv_det;
  tmp.mat[3][2] = (-matrix.mat[3][0] * a01a12_a02a11 +
    matrix.mat[3][1] * a00a12_a02a10 -
    matrix.mat[3][2] * a00a11_a01a10) * inv_det;
  tmp.mat[3][3] = (matrix.mat[2][0] * a01a12_a02a11 -
    matrix.mat[2][1] * a00a12_a02a10 +
    matrix.mat[2][2] * a00a11_a01a10) * inv_det;
  return tmp;
}

export function CreateRendererTexture(gl, resolution) {
  let frame = gl.createFramebuffer();
  gl.bindFramebuffer(gl.FRAMEBUFFER, frame);

  let depth = gl.createRenderbuffer();
  gl.bindRenderbuffer(gl.RENDERBUFFER, depth);
  gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, resolution[0], resolution[1]);
  gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, depth);

  let texture = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_2D, texture);
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, resolution[0], resolution[1], 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
  gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture, 0);

  gl.bindTexture(gl.TEXTURE_2D, null);
  gl.bindRenderbuffer(gl.RENDERBUFFER, null);
  gl.bindFramebuffer(gl.FRAMEBUFFER, null);

  return {
    "frame": frame,
    "depth": depth,
    "texture": texture,
    "resolution": resolution
  }
}

export class TextureScreen {
  #vertex_source =
    `#version 300 es
      layout (location = 0) in vec3 _vp;
      layout (location = 1) in vec3 _vn;
      layout (location = 2) in vec3 _vc;
      out vec2 vt;
      void main(void) {
        gl_Position = vec4(_vp, 1.0);
        vt = _vc.xy;
      }`;
  #fragment_source =
    `#version 300 es
      precision mediump float;
      uniform sampler2D _texture;
      in vec2 vt;
      layout (location = 0) out vec4 _color;
      void main(void) {
        _color = texture(_texture, vt);
      }`;
  #program = null;
  #vao = null;
  #object = null;
  #uniform = null;
  constructor(gl) {
    const vertex_shader = CreateVertexShader(gl, this.#vertex_source);
    const fragment_shader = CreateFragmentShader(gl, this.#fragment_source);
    this.#program = CreateProgram(gl, vertex_shader, fragment_shader);
    this.#vao = gl.createVertexArray();
    gl.bindVertexArray(this.#vao);
    this.#object = CreatePanelObject(gl);
    gl.bindVertexArray(null);
    this.#uniform = gl.getUniformLocation(this.#program, "_texture");
  }
  Draw(gl, texture, resolution) {
    let scale = Math.floor(Math.min(gl.drawingBufferWidth / resolution[0],
      gl.drawingBufferHeight / resolution[1]));
    let draw_res = resolution.map((n) => { return n * scale; });
    let origin = [Math.floor((gl.drawingBufferWidth - draw_res[0]) / 2),
    Math.floor((gl.drawingBufferHeight - draw_res[1]) / 2)];
    gl.viewport(origin[0], origin[1], draw_res[0], draw_res[1]);
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    gl.useProgram(this.#program);
    gl.bindVertexArray(this.#vao);
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.uniform1i(this.#uniform, 0);
    gl.drawElements(gl.TRIANGLES, this.#object.index_len, gl.UNSIGNED_INT, 0);
  }

};