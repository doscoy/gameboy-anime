"use strict";

export function DecodePixel(tile_bin) {
  function GetPixel(line, col) {
    return ((((line & 0xFF) & (1 << col)) >>> col)<< 1) |
           ((((line & 0xFF00) >>> 8) & (1 << col)) >>> col);
  }
  const row_len = tile_bin.byteLength / 2;
  const byte_len = Math.floor(row_len) * 2;
  const pixel = new Uint8Array(byte_len * 4);
  let counter = 0;
  for (let row = 0; row < row_len; ++row) {
    const line = new DataView(tile_bin).getUint16(row * 2);
    for (let col = 7; col >= 0; --col) pixel[counter++] = GetPixel(line, col);
  }
  return pixel;
}

export function EncodeTileBinary(pixel_array) {
  const bit_len = 8;
  const len = pixel_array.length;
  const buffer = new ArrayBuffer(Math.ceil(len / 4));
  const array = new Uint8Array(buffer);
  let index = 0;
  let pixel = 0;
  let lo_buffer = 0;
  let hi_buffer = 0;
  for (let i = 0; i < len; ++i) {
    const bit = (bit_len - 1) - pixel;
    const lo = pixel_array[i] & 1;
    const hi = (pixel_array[i] & 2) >>> 1;
    lo_buffer |= lo << bit;
    hi_buffer |= hi << bit;
    if (++pixel >= bit_len) {
      pixel = 0;
      array[index++] = lo_buffer;
      array[index++] = hi_buffer;
      lo_buffer = 0;
      hi_buffer = 0;
    }
  }
  if (pixel != 0) {
    array[index++] = lo_buffer;
    array[index++] = hi_buffer;
  }
  return buffer;
}