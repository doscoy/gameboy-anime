"use strict";
import { LayerCanvas } from "./layer_canvas.mjs";
import { MouseDrag } from "./mousedrag.mjs";

export class VideoCanvas {
  #target = null;
  #wrap = document.createElement("div");
  #video = document.createElement("video");
  #canvases;
  #contexts;
  #external_callback = {load : null, ope : null, draw : null};
  #zoom = 1.0;
  #scroll = {x : 0.0, y : 0.0};
  #drag = new MouseDrag();
  #fps;
  #length = 60;
  #head = 0;
  #loop_id = null;

  constructor (div, resolution, fps = 60, init_video = null) {
    this.#target = div;
    this.#target.appendChild(this.#wrap);
    this.#video.muted = true;
    this.#video.preload = "auto";
    this.#canvases = new LayerCanvas(this.#wrap, resolution[0], resolution[1], 2);
    this.#canvases.addEventListener("mousedown", event => this.#MousedownCallback(event));
    this.#canvases.addEventListener("mousemove", event => this.#MousemoveCallback(event));
    this.#canvases.addEventListener("mouseup", event => this.#MouseupCallback(event));
    this.#canvases.addEventListener("mouseleave", event => this.#MouseupCallback(event));
    this.#canvases.addEventListener("wheel", event => this.#WheelCallback(event));
    this.#contexts = this.#canvases.GetContexts("2d");
    this.#fps = fps;
    this.#DrawGrid();
    this.#video.addEventListener("loadeddata", () => this.#LoadVideoCallback());
    if (init_video != null) this.#SetVideoSourceByURL(init_video);
  }

  async #SetVideoSourceByURL(url) {
    await fetch(url)
      .then(responce => responce.blob())
      .then(data => {
        const reader = new FileReader();
        reader.onload = (event) => this.SetVideoSource(event.target.result);
        reader.readAsDataURL(data);
      });
  }

  #DrawGrid() {
    const grid_size = 8;
    const grid_x = Math.floor(this.#canvases.GetWidth() / 8);
    const grid_y = Math.floor(this.#canvases.GetHeight() / 8);
    const grid_len = grid_x * grid_y;
    this.#contexts[0].fillStyle = "lightgray";
    for (let grid = 0; grid < grid_len; ++grid) {
      const x = (grid % grid_x);
      const y = Math.floor(grid / grid_x);
      const fix = grid_x % 2 == 1 ? 0 :
                  y % 2 == 0 ? 0 :
                  1;
      if (((grid + fix) % 2) == 0) {
        this.#contexts[0].fillRect(x * grid_size, y * grid_size, grid_size, grid_size);
      }
    }
  }

  #CalcImagePosition() {
    const canvas_width = this.#canvases.GetWidth();
    const canvas_height = this.#canvases.GetHeight();
    const scale_x = canvas_width / this.#video.videoWidth;
    const scale_y = canvas_height / this.#video.videoHeight;
    const pos_x = (scale_x <= scale_y ? 0 : Math.abs(this.#video.videoWidth * scale_y - canvas_width) * 0.5) + this.#scroll.x;
    const pos_y = (scale_y <= scale_x ? 0 : Math.abs(this.#video.videoHeight * scale_x - canvas_height) * 0.5) + this.#scroll.y; 
    const scale = Math.min(scale_x, scale_y) * this.#zoom;
    const size_x = this.#video.videoWidth * scale;
    const size_y = this.#video.videoHeight * scale;
    return {pos : { x : pos_x, y : pos_y}, size : {x : size_x, y : size_y}};
  }

  #ResetTranspose() {
    this.#zoom = 1.0;
    this.#scroll.x = 0.0;
    this.#scroll.y = 0.0;
    if (this.#external_callback.ope != null) this.#CallExternalOperationCallback();
  }
  
  #DrawImage(property) {
    this.#contexts[1].clearRect(0, 0, this.#canvases.GetWidth(), this.#canvases.GetHeight());
    this.#contexts[1].drawImage(this.#video, property.pos.x, property.pos.y, property.size.x, property.size.y);
    if (this.#external_callback.draw != null) this.#external_callback.draw();
  }

  #LoadVideoCallback() {
    if (this.#external_callback.load != null) this.#external_callback.load(this.#video);
    this.#ResetTranspose();
    this.#DrawImage(this.#CalcImagePosition());
  }

  #CallExternalOperationCallback() {
    const property = this.#CalcImagePosition();
    const fix_zoom = this.#zoom;
    const fix_scroll = {x : this.#scroll.x / property.size.x, y : this.#scroll.y / property.size.y};
    this.#external_callback.ope(fix_zoom, fix_scroll);
  }

  #MousedownCallback(event) {
    if (event.buttons == 1) {
      this.#drag.MouseDown(event.x, event.y, this.#scroll.x, this.#scroll.y);
    } else if (event.buttons == 4) {
      this.#ResetTranspose();
      this.#DrawImage(this.#CalcImagePosition());
    }
  }

  #MousemoveCallback(event) {
    if (this.#drag.IsDraging()) {
      const data = this.#drag.GetDistance(event.x, event.y);
      this.#scroll.x = data.buffer.x;
      this.#scroll.y = data.buffer.y;
      if (this.#external_callback.ope != null) this.#CallExternalOperationCallback();
      this.#DrawImage(this.#CalcImagePosition());
    }
  }

  #MouseupCallback(event) {
    this.#drag.MouseUp();
  }

  #WheelCallback(event) {
    event.preventDefault();
    const property_prev = this.#CalcImagePosition();
    this.#zoom = Math.min(Math.max(.125, this.#zoom + event.deltaY * -0.001), 100);
    const property_temp = this.#CalcImagePosition();
    this.#scroll.x -= (property_temp.size.x - property_prev.size.x) / 2.0;
    this.#scroll.y -= (property_temp.size.y - property_prev.size.y) / 2.0;
    if (this.#external_callback.ope != null) this.#CallExternalOperationCallback();
    const property = this.#CalcImagePosition();
    this.#DrawImage(property);
  }

  #PlayLoop() {
    const interval_ms = 1000 / this.#fps;
    this.#video.loop = true;
    this.#video.play();
    this.#loop_id = setInterval(() => {
      const head = this.#video.duration * this.#head;
      const len = (1 / this.#fps) * this.#length;
      const tail = head + len;
      if (this.#video.currentTime < (head - (1 / this.#fps))) {
        this.#video.currentTime = head;
      } else if (this.#video.currentTime > tail) {
        this.#video.currentTime = head;
      }
      if (this.#external_callback.load != null) this.#external_callback.load(this.#video);
      this.#DrawImage(this.#CalcImagePosition());
    }, interval_ms);
  }

  #StopLoop() {
    this.#video.pause();
    clearInterval(this.#loop_id);
    this.#loop_id = null;
  }

  IsPlay() { return this.#loop_id != null; }

  Play() { if (!this.IsPlay()) this.#PlayLoop(); }

  Pause() { this.#StopLoop(); }

  async Stop(wait) {
    const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
    this.#StopLoop();
    const head = this.#video.duration * this.#head;
    this.#video.currentTime = head;
    this.#video.loop = false;
    await sleep(wait);
    if (this.#external_callback.load != null) this.#external_callback.load(this.#video);
    this.#DrawImage(this.#CalcImagePosition());
  }

  async NextFrame(wait) {
    const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
    if (this.IsPlay()) return;
    const now = this.#video.currentTime;
    const next = now + (1 / this.#fps);
    if (this.#video.ended) return false;
    this.#video.currentTime = next;
    await sleep(wait);
    if (this.#external_callback.load != null) this.#external_callback.load(this.#video);
    this.#DrawImage(this.#CalcImagePosition());
    return true;
  }

  GetFPS() { return this.#fps; }

  GetVideo() { return this.#video; }

  SetVideoSource(video) {
    if (this.#loop_id != null) this.#StopLoop();
    this.#video.src = video;
  }

  SetFPS(fps) {
    this.#fps = fps;
    if (this.#loop_id != null) {
      this.#StopLoop();
      this.#PlayLoop();
    }
  }

  SetLength(length) { this.#length = length; }

  SetPosition(position) { this.#head = position; }

  SetLoadVideoCallback(func) { this.#external_callback.load = func; }

  SetOperationCallback(func) { this.#external_callback.ope = func; }

  SetDrawCallback(func) { this.#external_callback.draw = func; }
};