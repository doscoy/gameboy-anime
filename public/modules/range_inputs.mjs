"use strict";

class MultiRange {
  #target;
  #wrap = document.createElement("div");
  #ranges = new Array();
  #input_callback = null;

  constructor(target, min, max, step, init) {
    this.#target = target;
    for (let i = 0; i < init.length; ++i) {
      const range = document.createElement("input");
      range.type = "range";
      range.style.border = "none";
      range.style.padding = "none";
      range.style.margin = "none";
      range.style.slider
      range.min = min;
      range.max = max;
      range.step = step;
      range.value = init[i];
      range.addEventListener("input", (event) => this.#InputCallback(event));
      this.#wrap.appendChild(range);
      if (i != (init.length - 1)) this.#wrap.appendChild(document.createElement("br"));
      this.#ranges.push(range);
    }
    this.#target.appendChild(this.#wrap);
  }

  #InputCallback(event) {
    this.#ranges.forEach((range, index) => {
      if (event.target === range) {
        if (index != 0) {
          range.value = Math.max(range.value, this.#ranges[index - 1].value);
        }
        if (index != (this.#ranges.length - 1)) {
          range.value = Math.min(range.value, this.#ranges[index + 1].value);
        }
      }
    });
    if (this.#input_callback != null) this.#input_callback();
  }

  SetCallback(func) { this.#input_callback = func; }

  get value() { return this.#ranges.map(range => range.value); }
  GetValues() { return this.value; }
}

export class RangeInputs {
  #target;
  #wrap = document.createElement("div");
  #ranges = new Array();
  #input_callback = null;

  constructor(target, recipe) {
    this.#target = target;
    for (let i = 0; i < recipe.length; ++i) {
      if (Array.isArray(recipe[i].init)) {
        const multi = new MultiRange(this.#wrap, recipe[i].min, recipe[i].max, recipe[i].step, recipe[i].init);
        multi.SetCallback(() => this.#InputCallback());
        this.#ranges.push(multi);
      } else {
        const range = document.createElement("input");
        range.type = "range";
        range.min = recipe[i].min;
        range.max = recipe[i].max;
        range.step = recipe[i].step;
        range.value = recipe[i].init;
        range.addEventListener("input", () => this.#InputCallback());
        this.#wrap.appendChild(range);
        if (i != (recipe.length - 1)) this.#wrap.appendChild(document.createElement("br"));
        this.#ranges.push(range);
      }
    }
    this.#target.appendChild(this.#wrap);
  }

  #InputCallback() { if (this.#input_callback != null) this.#input_callback(); }

  SetCallback(func) { this.#input_callback = func; }

  GetValues() {
    return this.#ranges.map(range => range.value);
  }
}